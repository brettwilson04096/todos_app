const express = require('express')
const router = express.Router()
const Joi = require('@hapi/joi')
const Todo = require('../models/todo')

//Show, new routes not needed

// Index route
router.get('/api/todos', (req, res) => {
  Todo.find({}, (err, docs) => {
    if(err) return res.send(err)
    res.json(docs)
  })
})

// Create Route
router.post('/api/todos', (req, res) => {
  const { action, completed } = req.body

  Todo.create({action: action, completed: completed}, (err) => {
    if(err) return res.send(err)
    res.redirect('/api/todos')
  })
})

// Destroy Route
router.delete('/api/todos/:id', (req, res) => { 
  let { id } = req.params
  
  Todo.findOneAndRemove({_id: id}, (err) => {
    if(err) return res.status(400).send('Whoops!')
    return res.redirect(303, '/api/todos')
  })
})

// Edit route

// Update route - update todo on change of complete and when title changes
router.put('/api/todos/:id', (req, res) => {
  let { id } = req.params
  let { action, completed } = req.body

  Todo.findByIdAndUpdate(
    id,
    {action: action, completed: completed},
    {new: true},
    (err, doc) => {
      if(err) return res.status(400).send('Whoops!')
      return res.json(doc)
    }
  )

})

router.patch('/api/todos/:id', (req, res) => {
  let { id } = req.params
  let payload = {}

  if(req.body.action != undefined) {
    payload = { action: req.body.action }
  }

  if(req.body.completed != undefined) {
    payload = { completed: req.body.completed }
  }

  Todo.findByIdAndUpdate(
    id,
    payload,
    {new: true},
    (err, doc) => {
      if(err) return res.status(400).send('Whoops!')
      return res.json(doc)
    }
  )
})

module.exports = router
