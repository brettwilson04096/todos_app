import React, { Component } from 'react'

class ToDo extends Component {
  dragHandlers = {
    dragStart: (e) => { 
      e.dataTransfer.setData('text/plain', this.props.todo._id)
      
    },
    dragOver: (e) => { e.preventDefault(); console.log('over:' + e.type) },
    drop: (e) => {
      this.props.dnd({
        dragId: e.dataTransfer.getData('text/plain'),
        dropId: this.props.todo._id
      })
    }
  }

  editAction = (e) => {
    if(this.props.todo.completed) { return }
    let el = e.target
    el.setAttribute('contenteditable', true)
    el.focus()
    el.setAttribute('style', 'border: 1px solid grey; color: red; padding: 2px 8px;')
    el.addEventListener('blur', this.updateAction)
    el.addEventListener('keypress', this.updateAction)
  }

  updateAction = (e) => {
    if((e.type == 'blur') || (e.type == 'keypress' && e.code == 'Enter')) {
      let el = e.target
      el.setAttribute('contenteditable', false)
      el.removeAttribute('style')
      el.removeEventListener('blur', this.updateAction)
      el.removeEventListener('keypress', this.updateAction)
      if(el.innerHTML != this.props.todo.action) {
        this.props.update.action({id: this.props.todo._id, action: el.innerHTML})
      }
    }
  }

  updateStatus = () => {
    this.props.update.status({id: this.props.todo._id, completed: !this.props.todo.completed})
  }

  handleDelete = () => { this.props.deleteTodo(this.props.todo._id) }

  render() {
    if(!this.props.filterComplete || !this.props.todo.completed) {
      return (
        <li className="todo" draggable="true" onDragStart={this.dragHandlers.dragStart} onDragOver={this.dragHandlers.dragOver} onDrop={this.dragHandlers.drop}>
          <button className="changeStatus" onClick={ this.updateStatus }>{ this.props.todo.completed ? 'Re-open' : 'Complete' }</button>
          <button className="delete" onClick={ this.handleDelete }>X</button>
          <span className={'action' + (this.props.todo.completed ? ' completed' : '')} onClick={ this.editAction }>{ this.props.todo.action }</span>
        </li>
      )
    } else {
      return null
    }
  }
}

export default ToDo
