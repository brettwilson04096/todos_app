const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
  action: { type: String, required: [true, 'You must provide an action.'] },
	completed: { type: Boolean, required: true }
}, { versionKey: 'version' })

const Todo = mongoose.model('Todo', TodoSchema);

module.exports = Todo;
