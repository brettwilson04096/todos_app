import React, { Component } from 'react'
import ToDo from './todo'

class ToDoList extends Component {
  render() {
    return (
      <ul className="todos">
        {
          this.props.todos.map((todo) => {
            return (
              <ToDo
                key={todo._id}
                todo={todo}
                filterComplete={this.props.filterComplete}
                update={this.props.update}
                deleteTodo={this.props.deleteTodo}
                dnd={this.props.dnd}
              />
            )
          })
        }
      </ul>
    )
  }
}

export default ToDoList
