import React, { Component } from 'react'

class SortMenu extends Component {
  state = {
    sortBy: this.props.sortBy,
    sortDir: this.props.sortDir
  }

  handleSortBy = (e) => {
    let val = e.target.innerHTML.toLowerCase();
    this.setState({sortBy: val}, () => {
      this.props.handleSort({sortBy: this.state.sortBy, sortDir: this.state.sortDir})
    })
  }

  handleSortDir = (e) => {
    let val = e.target.innerHTML.toLowerCase();
    this.setState({sortDir: val}, () => {
      this.props.handleSort({sortBy: this.state.sortBy, sortDir: this.state.sortDir})
    })
  }

  render() {
    return (
      <div id="sort_menu">
        <button>Sort by: <span id="sort_caret"></span></button>
        <ul>
          <li className={this.state.sortBy == 'action' ? 'active' : ''} onClick={this.handleSortBy}>Action</li>
          <li className={this.state.sortBy == 'completed' ? 'active' : ''} onClick={this.handleSortBy}>Completed</li>
          <li className={this.state.sortDir == 'asc' ? 'active' : ''} onClick={this.handleSortDir} id="asc">Asc</li>
          <li className={this.state.sortDir == 'desc' ? 'active' : ''} onClick={this.handleSortDir}>Desc</li>
        </ul>
      </div>
    )
  }
}

export default SortMenu
