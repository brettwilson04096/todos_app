function sortTodos({sortBy, sortDir, data}) {
  const orderDir = sortDir == 'asc' ? 1 : -1
  data.sort((a,b) => {
    let res = (a[sortBy].toString().toLowerCase() < b[sortBy].toString().toLowerCase()) ? (-1 * orderDir) : (1 * orderDir)
    return res
  })
  return data
}

module.exports.sortTodos = sortTodos
