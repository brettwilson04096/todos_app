import React, { useState } from 'react'

const AddTodo = (props) => {
  const [action, setAction] = useState('');

  const handleChange = (e) => {
    setAction(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if(action) {
      props.addTodo(action)
      setAction('')
    }
  }

  return (
    <form onSubmit={handleSubmit} className="addTodoForm">
      <div>
        <input type="text" name="action" onChange={handleChange} value={action} placeholder="Add new todo item" />
        <button>Submit</button>
      </div>
    </form>
  )
}

export default AddTodo
