/****************************************
 * app.js
 * 1) sets itself up from express and
 * 2) adds middleware
 * 3) establishes paths
 * 4) 
 ****************************************/

const express = require('express')
const app = express()
const path = require('path')
const login = require('./routes/login')
const todos = require('./routes/todos')
require('dotenv').config()

//view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(express.urlencoded())
app.use(express.json())

//routes
app.use(login)
app.use(todos)

module.exports = app
