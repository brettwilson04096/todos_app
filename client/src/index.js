import React, { Component } from 'react'
import { render } from 'react-dom'
import axios from 'axios'
import FilterComplete from './components/filterComplete'
import SortMenu from './components/sortMenu'
import AddTodo from './components/addTodo'
import ToDoList from './components/todoList'
import { sortTodos } from './helpers/helpers'
import './styles/main.scss'

class App extends Component {
  state = {
    todos: [],
    filterComplete: false,
    sortBy: 'action',
    sortDir: 'asc'
  }

  componentDidMount() {
    axios.get('/api/todos')
    .then((response) => {
      return sortTodos({
        sortBy: this.state.sortBy,
        sortDir: this.state.sortDir,
        data: response.data
      })
    })
    .then((sortedData) => this.setState({todos: sortedData}))
    .catch((err) => { console.log(err) })
  }

  handleDragStart = (id) => {
    this.setState({dragStartTgt: id})
  }

  handleDrop = ({dragId, dropId}) => {
    //setup tmp todos
    let todos = this.state.todos

    //search tmp todos for indexes of drag and drop ids
    let dragIndex = todos.findIndex((todo) => {
      return todo._id == dragId
    })

    let dropIndex = todos.findIndex((todo) => {
      return todo._id == dropId
    })

    //splice out the dragged todo
    let dragTodo = todos.splice(dragIndex, 1)

    //splice back in the drag target at the drop location
    todos.splice(dropIndex, 0, dragTodo[0])

    //setstate with updated array of todos
    this.setState({todos: todos, sortBy: '', sortDir: ''})
  }

  addTodo = (newAction) => {
    const newTodo = { action: newAction, completed: false }

    const options ={
      url: '/api/todos',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      data: newTodo
    }

    axios(options)
      .then((response) => { return response.data })
      .then((allTodos) => {
        let newTodoWithID = allTodos.find((todo) => {
          return todo.action === newTodo.action
        })

        //only sort when drag operations haven't occured
        if(this.state.sortBy && this.state.sortDir) {
          let sortedTodos = sortTodos({
            sortBy: this.state.sortBy,
            sortDir: this.state.sortDir,
            data: allTodos
          })
          return sortedTodos
        } else {
          let tempTodos = this.state.todos
          tempTodos.unshift(newTodoWithID)
          return tempTodos
        }
      })
      .then((todos) => this.setState({todos: todos}))
      .catch((err) => { console.log(err) })
  }

  updateAction = ({id, action}) => {
    //make patch request to database
    const options = {
      url: `/api/todos/${id}`,
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      data : { action: action }
    }

    axios(options)
      .then((response) => { return response.data })
      .then((data) => {
        if(id == data._id) {
          let tempTodos = this.state.todos
          let index = tempTodos.findIndex(todo => todo._id == data._id)
          tempTodos[index].action = action

          //only sort when drag operations haven't occured
          if(this.state.sortBy && this.state.sortDir) {
            tempTodos = sortTodos({
              sortBy: this.state.sortBy,
              sortDir: this.state.sortDir,
              data: tempTodos
            })
          }

          this.setState({todos: tempTodos})
        }
      })
      .catch((err) => { console.log(err) })
  }

  updateStatus = ({id, completed}) => {
    //make patch request to database
    const options = {
      url: `/api/todos/${id}`,
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      data : { completed: completed }
    }

    axios(options)
      .then((response) => { return response.data })
      .then((data) => {
        if(id == data._id) {
          let tempTodos = this.state.todos
          let index = tempTodos.findIndex(todo => todo._id == data._id)
          tempTodos[index].completed = completed

          //only sort when drag operations haven't occured
          if(this.state.sortBy && this.state.sortDir) {
            tempTodos = sortTodos({
              sortBy: this.state.sortBy,
              sortDir: this.state.sortDir,
              data: tempTodos
            })
          }

          this.setState({todos: tempTodos})
        }
      })
      .catch((err) => { console.log(err) })
  }

  deleteTodo = (id) => {
    const options = {
      url: `/api/todos/${id}`,
      method: 'DELETE'
    }

    axios(options)
      .then((response) => {
        //response data is junk because it is not sorted - must filter out the todo from original state
        let tempTodos = this.state.todos
        let filteredTodos = tempTodos.filter((todo) => {
          if(todo._id != id) {
            return todo
          }
        })
        this.setState({todos: filteredTodos})
      })
      .catch((err) => { console.log(err) })
  }

  handleFiltered = (val) => {
    this.setState({ filterComplete: val })
  }

  handleSort = ({sortBy, sortDir}) => {
    let sortedTodos = sortTodos({
      sortBy: sortBy,
      sortDir: sortDir,
      data: this.state.todos
    })

    this.setState({todos: sortedTodos, sortBy: sortBy, sortDir: sortDir})
  }

  render() {
    return (
      <div>
        <h1>Brett's Todo List</h1>
        <AddTodo addTodo={this.addTodo} />
        <FilterComplete handleFiltered={this.handleFiltered} />
        <SortMenu
          handleSort={this.handleSort}
          sortBy={this.state.sortBy}
          sortDir={this.state.sortDir} />
        <ToDoList
          todos={this.state.todos}
          filterComplete={this.state.filterComplete}
          update={{action: this.updateAction, status: this.updateStatus}}
          deleteTodo={this.deleteTodo}
          dnd={this.handleDrop}
        />
      </div>
    );
  }
}

render(
  <App />,
  document.getElementById("root")
)
