import React, { Component } from 'react'

class FilterComplete extends Component {
  render() {
    return (
      <form id="filter_completed">
        <label htmlFor="filter_box">Hide Completed:  </label>
        <input name="filter_box" type='checkbox' onChange={(e) => { this.props.handleFiltered(e.target.checked) }} />
      </form>
    )
  }
}

export default FilterComplete
