const express = require('express')
const router = express.Router()
const User = require('../models/user')

router.post('/api/login', (req, res) => {
  const { username, password } = req.body

  User.findOne({username: username, password: password }, (err, user) => {
    err ? res.send(err) : res.json(user) //res.json returns null if no user found
  })
})

module.exports = router
